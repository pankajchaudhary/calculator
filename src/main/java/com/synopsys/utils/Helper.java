package com.synopsys.utils;

import java.util.ArrayList;
import java.util.List;

public class Helper {

	public static final List<String> ignoreTokens = new ArrayList<String>();

	static {
		ignoreTokens.add(",");
	}

	public enum Operators {
		LET("LET"), ADD("ADD"), SUB("SUB"), MULT("MULT"), DIV("DIV");
		private String operator;

		private Operators(String operator) {
			this.operator = operator;
		}

		public String getOperator() {
			return operator;
		}

	}

	public enum Scope {
		OPENBRACKET("("), CLOSEBRACKET(")");
		private String scope;

		private Scope(String scope) {
			this.scope = scope;
		}

		public String getScope() {
			return scope;
		}

	}

	public static boolean isOperator(String operator) {
		return operator.equalsIgnoreCase(Operators.LET.name())
				|| operator.equalsIgnoreCase(Operators.ADD.name())
				|| operator.equalsIgnoreCase(Operators.SUB.name())
				|| operator.equalsIgnoreCase(Operators.MULT.name())
				|| operator.equalsIgnoreCase(Operators.DIV.name());
	}

	public static boolean isScope(String scope) {
		return scope.equalsIgnoreCase(Scope.OPENBRACKET.getScope())
				|| scope.equalsIgnoreCase(Scope.CLOSEBRACKET.getScope());
	}

}
