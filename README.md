# README #

**### What is this repository for? ###**

This project is for a command line calculator.

**### How do I get set up? ###**


1. Clone this repository and download the code. (OR download the code via https://bitbucket.org/pankajchaudhary/calculator/downloads)
2. Change dir to the location where pom.xml is present.
3. Run following command mvn clean install.
4. Change dir to target.
5. Execute the following command 
   java -jar calculator-jar-with-dependencies.jar "let(a, let(b, 10, add(b, b)), let(b, 20, 
   add(a, b))" 
 
**### Contribution guidelines ###**

There are 8 positive and negative test cases that will execute with each build process