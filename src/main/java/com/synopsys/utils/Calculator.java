package com.synopsys.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

/***
 * Calculator class will provide a CLI to perform basic addition, subtraction,
 * multiplication, division operation
 * 
 *@author PANKAJ.CHAUDHARY
 *
 */


/*
 * Assumption: 
 * 1. Calculator will only accept Integers, can be easily modified to access Double value too.
 * 2. Logging is set to CONSOLE and FILE
 * 
 * */

public class Calculator {

	final static Logger logger = Logger.getLogger(Calculator.class);

	public static void main(String[] args) {
		
		if(args.length==0){
			logger.error("Please enter the operation to perform");
			System.exit(0);
		}
		
		try{
			System.out.println("Result is " +doCalculation(args[0]));
		}catch(Exception e){
			logger.error(e);
			System.out.println(e.getMessage());
		}
	}

	
	public static Object doCalculation(String command) throws Exception {
		String uuid = UUID.randomUUID().toString();
		logger.info("Creating tokens for UUID =" + uuid +", command =" + command);
		logger.debug("Creating tokens for UUID =" + uuid + ", command ="+ command);

		List tokens = tokenizeString(command, uuid);

		if (tokens.isEmpty()) {
			logger.error("Error parsing the command");
			System.exit(0);
		}
		
		Object retValue = calculate(tokens, uuid);
		
		logger.info("Return Value for UUID =" + uuid +",  =" + retValue);
		logger.debug("Return Value for UUID =" + uuid +",  =" + retValue);
		return retValue;
	}
	
	
	/*
	 * Tokenize the command and return individual
	 */
	private static List tokenizeString(String command, String uuid) {

		List<String> tokens = new ArrayList<String>();

		command = command.replaceAll("\\s+", "");
		StringTokenizer token = new StringTokenizer(command, "( , )", true);
		while (token.hasMoreTokens()) {
			String tstr = token.nextToken();
			if (Helper.ignoreTokens.contains(tstr))
				continue;
			else
				tokens.add(tstr);
		}

		logger.info("Created tokens for UUID =" + uuid + ", token count ="
				+ tokens.size());
		logger.debug("Created tokens for UUID =" + uuid + ", tokens ="
				+ Arrays.toString(tokens.toArray()));

		return tokens;
	}

	/*
	 * Perform the calcuation based on tokens
	 */
	private static Object calculate(List<String> tokens, String uuid)  throws Exception{
		logger.info("Invoking calculate for UUID =" + uuid);
		logger.debug("Invoking calculate for UUID =" + uuid + ", tokens ="
				+ Arrays.toString(tokens.toArray()));

		Map<String, Integer> variables = new HashMap<String, Integer>();
		Stack<String> operator = new Stack<String>();
		Stack<String> scope = new Stack<String>();
		Stack<Object> value = new Stack<Object>();

		Iterator<String> itr = tokens.iterator();
		while (itr.hasNext()) {
			String s = itr.next();
			if (Helper.isOperator(s))
				operator.push(s);
			else if (Helper.isScope(s))
				scope.push(s);
			else
				value.push(s);
			execute(variables, operator, scope, value, uuid);
		}
		logger.info("calculated the result ="+ value.peek() +" for UUID =" + uuid);
		return value.pop();
	}

	
	private static void execute(Map<String, Integer> variables,
			Stack<String> operator, Stack<String> scope, Stack<Object> value, String uuid) throws Exception {
		logger.debug("Parameters for uuid "+uuid+", value =" +variables.toString() +", operator="+ operator
				+ ", scope "+ scope+", value ="+value);
		
		if(operator.isEmpty() || value.isEmpty())
			return;
		
		if(NumberUtils.isNumber(value.peek().toString()) && 
				operator.peek().equalsIgnoreCase(Helper.Operators.LET.getOperator())){
			operator.pop();
			Integer val = new Integer(value.pop().toString());
			variables.put(value.pop().toString(), val);
		}else if(scope.peek().equals(Helper.Scope.CLOSEBRACKET.getScope())
				&& !operator.isEmpty()){
			String action = operator.pop();
			//pop the combination of open and closed bracket, so twice
			scope.pop();
			scope.pop();
			Object val2 = value.pop();
			Object val1 = value.pop();
			Integer result = executeAction(action, val1, val2, variables, uuid);
			if(result==null)
				throw new Exception("Invalid operation in the parameter...");
			value.push(result);
			logger.debug("Return value post action for UUID = "+uuid +" value="+result);
			execute(variables, operator, scope, value, uuid);
		}else
			return;
	}
	
	
	private static Integer executeAction(String action, Object val1, Object val2, 
			Map<String, Integer> variables, String uuid) throws Exception{
		logger.debug("paramters for uuid ="+uuid+", action =" +action +", Value1="+ val1.toString()
				+ ", Value2 "+ val2.toString() +", variables ="+variables.toString());

		Integer v1, v2;
		
		v1 = (NumberUtils.isNumber(val1.toString())) ?  new Integer(val1.toString()) : variables.get(val1.toString()) ;
		v2 = (NumberUtils.isNumber(val2.toString())) ?   new Integer(val2.toString()) : variables.get(val2.toString()) ;
		
		if(v1==null || v2==null){
			throw new Exception("Incorrect variable name, please check the variable names");
		}
		
		if(action.equalsIgnoreCase(Helper.Operators.ADD.getOperator()))
			return v1 + v2;
		else if(action.equalsIgnoreCase(Helper.Operators.SUB.getOperator()))
			return v1 - v2;
		else if(action.equalsIgnoreCase(Helper.Operators.MULT.getOperator()))
			return v1 * v2;
		else if(action.equalsIgnoreCase(Helper.Operators.DIV.getOperator()))
			return v1 / v2;
		else
			return null;
	}

}
