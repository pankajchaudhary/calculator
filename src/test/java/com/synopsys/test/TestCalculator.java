package com.synopsys.test;

import junit.framework.TestCase;
import org.junit.Test;
import com.synopsys.utils.Calculator;


/***
 * Test cases to test the positive and negative test cases
 * @author CWR.PANKAJ.CHAUDHARY
 *
 */
public class TestCalculator {

	@Test
	public void testcase1() throws Exception{
		TestCase.assertEquals(3,  
				Calculator.doCalculation("add(1, 2)"));
	}
	
	
	@Test
	public void testcase2() throws Exception{
		TestCase.assertEquals(7,  
				Calculator.doCalculation("add(1, mult(2, 3))"));
	}

	@Test
	public void testcase3() throws Exception{
		TestCase.assertEquals(12,  
				Calculator.doCalculation("mult(add(2, 2), div(9, 3))"));
	}

	@Test
	public void testcase4() throws Exception{
		TestCase.assertEquals(10,  
				Calculator.doCalculation("let(a, 5, add(a, a))"));
	}

	@Test
	public void testcase5() throws Exception{
		TestCase.assertEquals(55,  
				Calculator.doCalculation("let(a, 5, let(b, mult(a, 10), add(b, a)))"));
	}

	@Test
	public void testcase6() throws Exception{
		TestCase.assertEquals(40,  
				Calculator.doCalculation("let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b))"));
	}

	
	/* Invalid operation name  ADD renamed to SUM*/
	@Test
	public void testcase7() {
		String message = null;
		try{
			Calculator.doCalculation("let(a, let(b, 10, sum(b, b)), let(b, 20, add(a, b))");
		}catch(Exception e){
			message = e.getMessage();
		}	
		TestCase.assertEquals("Invalid operation in the parameter...", message);
	}

	/* Invalid variable name, added a typo mistake rename b to bb*/
	@Test
	public void testcase8() throws Exception{
		String message = null;
		try{
			Calculator.doCalculation("let(a, let(bb, 10, add(b, b)), let(b, 20, add(a, b))");
		}catch(Exception e){
			message = e.getMessage();
		}	
		TestCase.assertEquals("Incorrect variable name, please check the variable names", message);
	}

	
}
